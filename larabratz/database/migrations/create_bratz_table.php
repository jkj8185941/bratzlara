<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bratz', function (Blueprint $table) {
            $table->id();
            $table->string('personaje',30); 
            $table->number('año', 4);
            $table->string('peliseri',30);
            $table->foreignId('id_coleccion')->constrained('colecciones')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bratz');
    }
};
